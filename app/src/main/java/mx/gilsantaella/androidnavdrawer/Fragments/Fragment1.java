package mx.gilsantaella.androidnavdrawer.Fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import mx.gilsantaella.androidnavdrawer.MainActivity;
import mx.gilsantaella.androidnavdrawer.R;

/**
 * Created by G1L21088 on 11/03/2016.
 */
public class Fragment1 extends Fragment implements OnMapReadyCallback {

    private static View view;
    private GoogleMap mMap;
    private static final double LATITUD = 19.3880376;
    private static final double LONGITUD = -99.1029591;

    public Fragment1() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null)
            return null;
        view = inflater.inflate(R.layout.fragment_fragment1, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
              .findFragmentById(R.id.location_map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMyLocationEnabled(true);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.addMarker(new MarkerOptions().position(new LatLng(LATITUD, LONGITUD)).title("My Home").snippet("Home Address"));
        mMap.moveCamera(CameraUpdateFactory
                .newLatLng(new LatLng(19.3880456, -99.1029327)));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
    }
}

